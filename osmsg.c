#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "asm/unistd.h"
#define _OPEN_SYS
#include <sys/types.h>
#include <pwd.h>
// gcc -m32 -o osmsg -I /u/OSLab/jmd149/linux-2.6.23.1/include/ osmsg.c -static

/* James Devine
	CS 1550
	Project 1
*/

struct message {
	uid_t user; //to or from
	char msg[140];
};

int main(int argc, char ** argv){
	struct passwd *p;
	struct passwd *pd;
	struct passwd *pass;
	char * user;
	char * message;
	int more_messages = 1;
	int result = 0;
	struct message m;
	struct message *ptr;
	uid_t current_user;
	ptr = &m;

	if(strcmp(argv[1], "-r") == 0){ //read the messages
	
		while(1){
			
			result = syscall(326, ptr); //syscall with a pointer to an empty message
			
			if(result < 0){  //no messages

				printf("\nNo messages\n");
				break;
				
			
			}
			else if(result == 0){
				//messages are done
				pass = getpwuid(ptr->user);
				
				printf("\nFrom %s : ", pass->pw_name);
				printf("%s\n", ptr->msg);
				printf("\n");
				
				break;
				
			}
			else if( result == 1){
				//more messages to come
				
				pass = getpwuid(ptr->user);
				
				printf("\nFrom %s : ", pass->pw_name);
				printf("%s", ptr->msg);
							
			}
		
		}
		
	}
	
	else if(strcmp(argv[1], "-s") == 0 && argc == 4){
	
		user = argv[2];					//get the name from the user
		
		 if ((p = getpwnam(user)) == NULL){	//check if the user exists
			//perror("getpwnam() error");
			printf("\nUser does not exist\n");
		 }
			
		else {		
			m.user = p->pw_uid;	 //sets the user's id  in the struct
			
			strncpy(m.msg, argv[3], 140);	//copy the message to the struct	
		
			result = syscall(325, ptr);		//make the syscall 
			
		}	
		
	}

	else{
		printf("\n Proper usage:");
		printf("\n To send messages -> osmsg -s jmd149  Hello world");
		printf("\n To get messages -> osmsg -r");
		
		return -1;
	}

	return 0;
}